'use strict';
//= ../node_modules/lazysizes/lazysizes.js
//= ../node_modules/jquery/dist/jquery.js
//= ../node_modules/popper.js/dist/umd/popper.js
//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js
//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js


$(document).ready(function () {

	$("#header-page__down").on("click","a", function (event) {
		event.preventDefault();
		var id  = $(this).attr('href'),
		top = $(id).offset().top;
		$('body,html').animate({scrollTop: top}, 900);
	});

	$(window).scroll(function (){
		scrollMenu();
	});

	function scrollMenu() {
		if ( $(window).width() > 991 ) {
			if ($(window).scrollTop() > 200) {
				$(".header-page__down").addClass("scroll");
			} else {
				$(".header-page__down").removeClass("scroll");
			}
		} else {
			$(".header-page__down").removeClass("scroll");
		}

	}
	scrollMenu();

});
